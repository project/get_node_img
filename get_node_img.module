<?php

/**
 * This file holds a Drupal module for image transfer.
 *
 * This module transfers images to the browser.  These images are attached to
 * Drupal nodes using CCK imagefield.  Images are requested using URLs of the
 * format -
 *        /node_resource/NODE_ID/CCK_LABEL/IMAGE_NUMBER[.EXTENSION]
 * Some example URLs are "/node_resource/10/stars/0",
 * "/node_resource/10/stars/1.png", "/node_resource/10/stars/1.hihi", etc.
 *
 * @package get_node_img
 * @author adnan@codesetter.com
 * @copyright Drupal Association
 * @license GPL v2
 */


/**
 * Name of the item stored in Drupal's "variable" table.  This variable holds
 * a file ID of an image.  This image is returned when a requested image could
 * not be found.
 */
define('GET_NODE_IMG_VAR_NAME', 'get_node_img_404_fid');


/**
 * Implementation of hook_menu()
 */
function get_node_img_menu()
{
    $items = array();

    /**
     * Sample URL:
     * node_img/NODE_ID/CCK_LABEL/0.png
     * Here CCK_LABEL is the machine readable label given to the
     * imagefield field, and 0 refers to the first image.
     */
    $items['node_resource'] = array(
        'title' => 'I spit image.',
        'page callback' => 'get_node_img',
        'type' => MENU_CALLBACK,
        'access arguments' => array('get_node_img'),
    );

    /**
     * Presents the 404 image selection form.
     */
    $items['admin/config/media/get_node_img_404_selection'] = array(
        'title'            => '404 Image Selection',
        'description'      => 'Select/unselect 404 image for the get_node_img module.',
        'page callback'    => 'drupal_get_form',
        'page arguments'   => array('get_node_img_404_selection'),
        'access arguments' => array('set_404_img'),
        'file'             => 'get_node_img.admin.inc',
    );

    return $items;
}
 

/**
 * Implementation of hook_permission()
 */
function get_node_img_permission()
{
    return array(
        'get_node_img' => array(
            'title'       => t('Fetch an image'),
            'description' => t('Allow users to fetch an image attached to a node.'),
        ),
        'set_404_img' => array(
            'title' => t('Set 404 image'),
            'description' => t('Allow users to upload a default image.'),
        )
    );
}


/**
 * Menu callback.
 *
 * Executes the actual image file transfer.  Emits HTTP 404 if no image can be
 * found using the arguments provided.
 *
 * @param string $nid
 *     This is the node ID.
 * @parma integer $cck_field_label
 *     The machine-readable label for the cck field.
 * @param integer $image_number
 *     0 for the first image, 1 for the second image, and so on.
 */
function get_node_img($nid = NULL, $cck_field_label = NULL, $image_number = 0)
{
    $img_field_name = 'field_' . $cck_field_label;
    $image_num = (int) $image_number;

    get_node_img_arg_validation($nid, $img_field_name, $image_num) ?
                                                TRUE : get_node_img_good_bye();

    $node_obj = node_load($nid);
    $fileinfo = $node_obj->{$img_field_name}['und'][$image_num];

    if ( ! empty($fileinfo))
    {
        $uri = $fileinfo['uri'];
        $mime = $fileinfo['filemime'];
        $filesize = $fileinfo['filesize'];
        $timestamp = $fileinfo['timestamp'];

        get_node_img_file_transfer($uri, $mime, $filesize, $timestamp);
    }
    // File not found ?!!!@#!!
    else
    {
        get_node_img_good_bye();
    }

    // Good bye :-)
    exit;
}


/**
 * URL argument validation function.
 *
 * @param string $nid
 * @param string $img_field_name
 * @param integer $img_num
 *     The first image has the number 0, the second one has 1, and so on...
 */
function get_node_img_arg_validation($nid, $img_field_name, $img_num)
{
    // Is the node id an integer?
    if ( ! ctype_digit($nid))
    {
        return FALSE;
    }

    // Does this node really exist?
    if ( ! ($node_obj = node_load($nid)))
    {
        return FALSE;
    }

    // Does this CCK image really exist?
    if ( ! isset($node_obj->{$img_field_name}['und'][$img_num]))
    {
        return FALSE;
    }

    return TRUE;
}


/**
 * Decides what to do if no image is found.  It either sends a preselected
 * image or HTTP 404.
 */
function get_node_img_good_bye()
{
    $img_404_fid = variable_get(GET_NODE_IMG_VAR_NAME, NULL);

    if ( ! empty($img_404_fid))
    {
        $img_404_info = file_load($img_404_fid);

        // Send the 404 image.
        get_node_img_file_transfer($img_404_info->uri,
                                   $img_404_info->filemime,
                                   $img_404_info->filesize,
                                   $img_404_info->timestamp);
        exit;
    }

    drupal_not_found();
    exit;
}


/**
 * Makes call to the caching and file tranfer functions.
 *
 * @param string $uri
 *    e.g. public://ref-email-msg.pn
 * @param string $mime
 *    e.g. image/png
 * @param integer $filesize
 *    File size in bytes.
 * @param integer $timestamp
 *    Unix timestamp of the given uri's last update.
 */
function get_node_img_file_transfer($uri, $mime, $filesize, $timestamp)
{
    $headers                   = array();
    $headers['Content-Type']   = mime_header_encode($mime);
    $headers['Content-Length'] = $filesize;

    // Get the right HTTP caching headers.
    // Also decide whether we need to transfer the file at all.
    _get_node_img_cache_set_cache_headers($timestamp, $headers) ?
                                        file_transfer($uri, $headers) : TRUE;
}


/**
 * Set file headers that handle "If-Modified-Since" correctly for the
 * given fileinfo. Most code has been taken from drupal_page_cache_header().
 *
 * @param string $timestamp
 * @param array $header
 * @return bool
 *     FALSE means there's no need to transfer the image, the browser already
 *     has an updated cached copy.
 */
function _get_node_img_cache_set_cache_headers($timestamp, &$headers)
{
    // Set default values:
    $last_modified = gmdate('D, d M Y H:i:s', $timestamp) .' GMT';
    $etag = '"'. md5($last_modified) .'"';

    // See if the client has provided the required HTTP headers:
    $if_modified_since = isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])
                         ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE'])
                         : FALSE;
    $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH'])
                     ? stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])
                     : FALSE;

    if ($if_modified_since AND $if_none_match
        AND $if_none_match == $etag // etag must match
        AND $if_modified_since == $last_modified)
    {
        // if-modified-since must match
        header('HTTP/1.1 304 Not Modified');

        // All 304 responses must send an etag if the 200 response
        // for the same object contained an etag
        header('Etag: '. $etag);

        // We must also set Last-Modified again, so that we overwrite Drupal's
        // default Last-Modified header with the right one
        header('Last-Modified: '. $last_modified);
        return FALSE;
    }

    // Send appropriate response:
    $headers['Last-Modified'] = $last_modified;
    $headers['ETag']          = $etag;

    return TRUE;
}


